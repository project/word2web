<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:v="urn:schemas-microsoft-com:vml"
  xmlns:o="urn:schemas-microsoft-com:office:office"
  xmlns:w="urn:schemas-microsoft-com:office:word"
  xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
  exclude-result-prefixes="v"
  >

<xsl:output method="html"
  omit-xml-declaration="yes"
  encoding="UTF-8"
  indent="yes" />

<xsl:strip-space elements="*"/>

<xsl:template match="html" priority="2">
  <xsl:apply-templates select="./*"/>
</xsl:template>

<xsl:template match="body" priority="2">
  <div>
    <xsl:apply-templates select="./*"/>
  </div>
</xsl:template>

<xsl:template match="*">
  <xsl:choose>
    <xsl:when test='name() = "style" or name() = "head" or name() = "title"'>
    </xsl:when>
    <xsl:otherwise>
      <xsl:element name="{name()}">
        <xsl:apply-templates select="* | @* | text()"/>
      </xsl:element>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="@*">
  <!-- src is allowed so that img tags which became addr tags can have it -->
  <xsl:if test='name(.) = "class" or name(.) = "id" or name(.) = "src"'>  
    <xsl:choose>
      <xsl:when test='. = "MsoNormal"'>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="{name(.)}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:if>
</xsl:template>

<!-- Convert proprietary semantic tags to spans with classes -->
<xsl:template match="
  st1:country-region |
  st1:place |
  street |
  address |
  place |
  city |
  country-region |
  personname |
  date |
  placetype |
  placename |
  state |
  sn
  " priority="3">
  <xsl:value-of select='.' />
</xsl:template>

<!-- Let's remove superscripts -->
<xsl:template match="sup" priority="3">
  <xsl:apply-templates />
</xsl:template>

<!-- The p class rules -->
<xsl:template match='p' priority='1'>
  <xsl:choose>
    <xsl:when test="@class='MsoToc1' or @class='MsoToc2' or @class='MsoToc3' or @class='MosToc4'">
      <xsl:variable name='toc_class'>
        <xsl:choose>
          <xsl:when test="@class='MsoToc1'">toc-1</xsl:when>
          <xsl:when test="@class='MsoToc2'">toc-2</xsl:when>
          <xsl:when test="@class='MsoToc3'">toc-3</xsl:when>
          <xsl:when test="@class='MsoToc4'">toc-4</xsl:when>
        </xsl:choose>
      </xsl:variable>
      <div class='toc {$toc_class}'>
        <xsl:for-each select="*//text()">
          <xsl:if test='string-length(.) &gt; 2'>
            <xsl:value-of select='.' />
          </xsl:if>
        </xsl:for-each>
      </div>
    </xsl:when>
    <xsl:when test="@class='MsoTitle'">
      <h1>
        <xsl:apply-templates />
      </h1>
    </xsl:when>
    <xsl:when test="@class='MsoSubtitle'">
      <h2>
        <xsl:apply-templates />
      </h2>
    </xsl:when>
    <xsl:when test="@class='blockquote' or @class='MsoBlockText'">
      <blockquote>
        <xsl:apply-templates />
      </blockquote>
    </xsl:when>
    <xsl:when test="@class='caption'">
      <p class='caption'>
        <xsl:apply-templates />
      </p>
    </xsl:when>
    <xsl:otherwise>
      <p>
      <xsl:apply-templates />
      </p>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- img tags @TODO: take these out -->
<xsl:template match='img' priority='1'>
  <img src='{@src}' />
</xsl:template>

<xsl:template match="a" priority="1">
  <xsl:choose>
    <xsl:when test='@href'>
      <a href="{@href}" name="{@name}">
        <xsl:apply-templates />
      </a>
    </xsl:when>
    <xsl:otherwise>
      <span>
        <xsl:apply-templates />
      </span>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- If span has a class, let's clear out its children -->
<xsl:template match="span" priority="1">
  <xsl:choose>
    <!--
    <xsl:when test='@style'>
      <xsl:choose>
        <xsl:when test='contains(@style, "display:none")'>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy><xsl:apply-templates /></xsl:copy>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    -->
    <xsl:when test='@class'>
      <xsl:choose>
        <xsl:when test='@class = "MsoHyperlink"'>
          <xsl:copy-of select='*/a/text()' />
        </xsl:when>
        <xsl:when test='@class = "MsoFootNoteReference"'>
          <a href="{../@href}" name="{../@name}">
            <sup><xsl:value-of select="." /></sup>
          </a>
        </xsl:when>
        <xsl:when test='@class = "Char"'>
          <xsl:value-of select='.' />
        </xsl:when>
        <xsl:when test='@class'>
          <span class='{@class}'><xsl:apply-templates /></span>
        </xsl:when>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:copy><xsl:apply-templates /></xsl:copy>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="img">
      <xsl:element name="{name()}">
        <xsl:apply-templates select="* | @* | text()"/>
      </xsl:element>
</xsl:template>

<xsl:template match="addr">
      <xsl:element name="{name()}">
        <xsl:apply-templates select="* | @* | text()"/>
      </xsl:element>
</xsl:template>

</xsl:stylesheet>
